import { Component } from '@angular/core';
import { AuthService } from '../auth-service.service';
import { ToolbarVisibilityService } from '../toolbar-visibility.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent {

  links= ['Food log', 'Weight tracker', 'Recipes', 'Diet'];

  constructor(
    public authService: AuthService,
    public toolbarVisibilityService: ToolbarVisibilityService)  {

  }

  async logout() {
    await this.authService.logout();
  }
}
