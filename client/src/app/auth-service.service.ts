import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { OktaAuthService } from '@okta/okta-angular';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _isAuthenticated = new BehaviorSubject<boolean>(false);
  private _isSignInInProgress = new BehaviorSubject<boolean>(false);

  public get isAuthenticated$() {
    return this._isAuthenticated.asObservable();
  }

  public get isSignInInProgress$() {
    return this._isSignInInProgress.asObservable();
  }

  constructor(private router: Router, private oktaAuth: OktaAuthService) {
    this.isAuthenticated()
      .then(authenticated => this._isAuthenticated.next(authenticated))
  }

  isAuthenticated(): Promise<boolean> {
    return this.oktaAuth.session.exists();
  }

  async handleLoginRedirect(): Promise<void> {
    await this.oktaAuth.handleLoginRedirect();
  }

  async login(username: string, password: string): Promise<void> {
    this._isSignInInProgress.next(true);
    try {
      const transaction = await this.oktaAuth.signInWithCredentials({ username, password });

      if (transaction.status !== 'SUCCESS') {
        throw Error('We cannot handle the ' + transaction.status + ' status');
      }


      const tokenResponse = await this.oktaAuth.token.getWithoutPrompt({
        sessionToken: transaction.sessionToken,
        responseType: ['token', 'id_token'],
        scopes: ['openid', 'profile'],
      });

      if(!tokenResponse.tokens.idToken){
        throw new Error('Missing id token');
      }
      this.oktaAuth.tokenManager.add('idToken', tokenResponse.tokens.idToken)

      if(!tokenResponse.tokens.accessToken){
        throw new Error('Missing access token');
      }
      this.oktaAuth.tokenManager.add('accessToken', tokenResponse.tokens.accessToken)

      this.oktaAuth.session.setCookieAndRedirect(transaction.sessionToken, environment.okta.redirectUri);

      this._isAuthenticated.next(true);

    }
    finally {
      this._isSignInInProgress.next(false);
    }
  }

  async logout(): Promise<void> {
    try {
      await this.oktaAuth.signOut();
      this._isAuthenticated.next(false);
      this.router.navigate(['/promo']);
    } catch (err) {
      console.error(err);
    }
  }

  getAccessToken(): string | undefined {
    return this.oktaAuth.getAccessToken();
  }
}
