import { TestBed } from '@angular/core/testing';

import { OktaWrapperService } from './okta-content.service';

describe('OktaWrapperService', () => {
  let service: OktaWrapperService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OktaWrapperService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
