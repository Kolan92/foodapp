import { Component, OnInit } from '@angular/core';
import { MatRadioChange } from '@angular/material/radio';
import { Theme, ThemeService } from '../theme.service';

@Component({
  selector: 'app-theme-switcher',
  templateUrl: './theme-switcher.component.html',
  styleUrls: ['./theme-switcher.component.scss']
})
export class ThemeSwitcherComponent implements OnInit {
  
  readonly themes = [Theme.Dark, Theme.Light];
  currentTheme!: Theme;

  constructor(public themeService: ThemeService) {  }

  ngOnInit() {
    this.themeService.currentTheme$.subscribe(theme => {
      this.currentTheme = theme;
    });
  }

  selectTheme(event: MatRadioChange) {
    this.themeService.setTheme(event.value as Theme);
  }
}
