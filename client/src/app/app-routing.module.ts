import { Injector, NgModule } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';
import { OktaAuthService } from '@okta/okta-angular';
import { OktaCallbackComponent } from './okta-callback/okta-callback.component';
import { PromoComponent } from './promo/promo.component';
import { AuthGuardService } from './auth-guard.service';


export function onAuthRequired(oktaAuth: OktaAuthService, injector: Injector) {
  const router = injector.get(Router);
  router.navigate(['/promo']);
}

const routes: Routes = [
  {
    path: 'promo',
    component: PromoComponent
  },
  {
    path: 'login/callback',
    component: OktaCallbackComponent
  },
  {
    path: 'food-log',
    loadChildren: () => import('./food-log/food-log.module').then(m => m.FoodLogModule),
    canActivate: [ AuthGuardService ],
  },  
  {
    path: '**',
    redirectTo: 'food-log'
  },
]

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    anchorScrolling: 'enabled'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
