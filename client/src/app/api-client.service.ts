import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AuthService } from './auth-service.service';

@Injectable({
  providedIn: 'root'
})
export class ApiClientService {

  constructor(private authService: AuthService, private httpClient: HttpClient) { }

  get<T>(url: string): Observable<T> {
    const options = this.getAuthorizationHeaders();
    return this.httpClient.get<T>(`${environment.weightTrackerBaseUrl}/${url}`, options);
  }

  private getAuthorizationHeaders() {
    const accessToken = this.authService.getAccessToken();
    return {headers: {'authorization': `Bearer ${accessToken}`} };
  }
}
