import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EntriesComponent } from './entries/entries.component';
import { MaterialModule } from '../material/material.module';
import { FoodLogRoutingModule } from './food-log-routing.module';


@NgModule({
  declarations: [EntriesComponent],
  imports: [
    MaterialModule,
    CommonModule,
    FoodLogRoutingModule
  ]
})
export class FoodLogModule { }
