import { NgModule } from '@angular/core'; 
import { Routes, RouterModule, Router } from '@angular/router';
import { EntriesComponent } from './entries/entries.component';

 
const routes: Routes = [
    {   
      path: '',   
      component: EntriesComponent, 
    }
  ];
 
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FoodLogRoutingModule { }
