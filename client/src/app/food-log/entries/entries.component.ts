import { Component, HostBinding, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiClientService } from 'src/app/api-client.service';
import { AuthService } from 'src/app/auth-service.service';

@Component({
  selector: 'app-entries',
  templateUrl: './entries.component.html',
  styleUrls: ['./entries.component.scss'],
  host: {
    class: 'entries content'

  }
})
export class EntriesComponent implements OnInit {
  dogs = ['Lunch', 'Dinner', 'Breakfast', 'Lunch', 'Dinner', 'Breakfast', 'Lunch', 'Dinner', 'Breakfast', 'Lunch', 'Dinner', 'Breakfast']

  entries!: Observable<any>;
  constructor(private apiClient: ApiClientService) { }
  
  ngOnInit(): void {
    this.entries = this.apiClient.get('food');
  }

}
