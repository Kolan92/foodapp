import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { BehaviorSubject, Observable } from 'rxjs';

export enum Theme{
  Dark = 'Dark',
  Light = 'Light'
}
@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  private readonly themeCookieName = 'theme';

  constructor(private cookieService: CookieService) { }

  private currentTheme = new BehaviorSubject<Theme>(this.getThemeFromCookie());
  get currentTheme$(): Observable<Theme> {
    return this.currentTheme.asObservable();
  }

  setTheme(theme: Theme): void {
    this.cookieService.set(this.themeCookieName, theme, {path: '/'});
    this.currentTheme.next(theme);
  }

  private getThemeFromCookie(): Theme {
    const cookieValue = this.cookieService.get(this.themeCookieName);
    return cookieValue as Theme || Theme.Light;
  }
}
