import { Injectable } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ToolbarVisibilityService {

  isVisible$!: Observable<boolean>;

  constructor(private router: Router) {
    this.isVisible$ = router.events.
      pipe(
        filter(event => event instanceof NavigationEnd),
        map(event => !(event as NavigationEnd).url.includes('promo')));
  }
}
