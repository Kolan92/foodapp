import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import {
  OKTA_CONFIG,
  OktaAuthModule,
  OktaConfig
} from '@okta/okta-angular';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { environment } from 'src/environments/environment';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { PromoComponent } from './promo/promo.component';
import { MaterialModule } from './material/material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ThemeSwitcherComponent } from './theme-switcher/theme-switcher.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OktaCallbackComponent } from './okta-callback/okta-callback.component';
import { CookieService } from 'ngx-cookie-service';
import { KebabCasePipe } from './kebab-case.pipe';
import { HttpClientModule } from '@angular/common/http';
import { Router } from '@angular/router';

function onAuthRequired(oktaAuth: any, injector: any) {
  // Use injector to access any service available within your application
  const router = injector.get(Router);
 
  // Redirect the user to your custom login page
  router.navigate(['/promo']);
}

const config: OktaConfig = {
    pkce: true,
    issuer: environment.okta.issuer,
    clientId: environment.okta.clientId,
    redirectUri: environment.okta.redirectUri,
    devMode: !environment.production,
    tokenManager: {
      autoRenew: true
    },
    onAuthRequired: onAuthRequired,
    scopes: ['openid', 'profile', 'email'],
    responseType: 'id_token token'
  }

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ToolbarComponent,
    PromoComponent,
    ThemeSwitcherComponent,
    OktaCallbackComponent,
    KebabCasePipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    OktaAuthModule,
    MaterialModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    OktaAuthModule
  ],
  providers: [    
    CookieService,
    { provide: OKTA_CONFIG, useValue: config }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() { }
 }
