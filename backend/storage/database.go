package storage

import (
	"food-app/models"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)
var db *gorm.DB

func InitDatabase() {
	println("Initialising database")
	dsn := "host=0.0.0.0 user=postgres password=postgres dbname=FoodApp port=5432 sslmode=disable"
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		panic("failed to connect database")
	}

	// Migrate the schema
	db.AutoMigrate(&models.FoodEntry{})

	db.Create(&models.FoodEntry{MealType: "Lunch", Calories: 800, Fats: 30, Carbohydrates: 45, Proteins: 20})
}

func GetAllFoodEntries() []models.FoodEntry {
	var entries []models.FoodEntry

	db.Find(&entries)
	return entries
}
