package models

import "gorm.io/gorm"

type FoodEntry struct {
	gorm.Model
	Calories      uint
	Proteins      uint
	Carbohydrates uint
	Fats          uint
	MealType      string
}
