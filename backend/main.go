package main

import (
	"food-app/routes"
	"food-app/storage"
	"github.com/gin-gonic/gin"
)

func init() {
	storage.InitDatabase()
}

func main() {
	router := gin.Default()
	router.GET("/health", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"health": "ok",
		})
	})
	v1 := router.Group("/api/v1/food-log")
	{
		v1.POST("/", routes.CreateFoodLog)
		v1.GET("/", routes.GetAllFodLog)
		v1.GET("/:id", routes.GetGoodLogById)
		v1.PUT("/:id", routes.UpdateFoodLog)
		v1.DELETE("/:id", routes.DeleteFoodLog)
	}

	router.Run() // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}
