using System;
using System.Collections.Generic;

namespace WeightTracker.Persistence.Models
{
    public class UserProfile
    {
        public int Id { get; set; }
        public string ExternalUserId { get; set; }
        public DateTime BirthDate { get; set; }
        public double Height { get; set; }
        public ICollection<WeightMeasurement> Measurements { get; set; }
    }
}