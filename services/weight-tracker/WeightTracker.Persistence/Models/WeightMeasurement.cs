using System;

namespace WeightTracker.Persistence.Models
{
    public class WeightMeasurement
    {
        public int Id { get; set; }
        public DateTime MeasuredOn { get; set; }
        public double Weight { get; set; }

        public UserProfile UserProfile { get; set; }
    }
}