using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WeightTracker.Persistence.Models;

namespace WeightTracker.Persistence
{
    internal sealed class UserProfileRepository : IUserProfileRepository
    {
        private readonly WeightTrackerDbContext dbContext;

        public UserProfileRepository(WeightTrackerDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public Task<UserProfile> Get(string externalUserId)
        {
            return dbContext.Profiles.SingleOrDefaultAsync(p => p.ExternalUserId == externalUserId);
        }

        public async Task Upsert(UserProfile userProfile)
        {
            await dbContext.Profiles.Upsert(userProfile)
                .On(p => p.ExternalUserId)
                .WhenMatched(p => new UserProfile
                {
                    Height = userProfile.Height,
                    BirthDate = userProfile.BirthDate
                })
                .RunAsync();

            await dbContext.SaveChangesAsync();
        }
    }
}