using Microsoft.Extensions.DependencyInjection;

namespace WeightTracker.Persistence.DependencyInjection
{
    public static class DependencyInjectionExtensions
    {
        public static void AddPersistenceDependencies(this IServiceCollection services)
        {
            services.AddTransient<IMeasurementsRepository, MeasurementsRepository>();
            services.AddTransient<IUserProfileRepository, UserProfileRepository>();
        }
    }
}