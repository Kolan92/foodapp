using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WeightTracker.Persistence.Models;

namespace WeightTracker.Persistence
{
    public class MeasurementsRepository : IMeasurementsRepository
    {
        private readonly WeightTrackerDbContext dbContext;

        public MeasurementsRepository(WeightTrackerDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public async Task<IEnumerable<WeightMeasurement>> Get(string externalUserId, int skip = 0, int take = 100)
        {
            var userProfile = await dbContext.Profiles
                .SingleOrDefaultAsync(p => p.ExternalUserId == externalUserId);

            return await dbContext.Entry(userProfile)
                .Collection(profile => profile.Measurements)
                .Query()
                .OrderBy(entry => entry.MeasuredOn)
                .Skip(skip)
                .Take(take)
                .ToListAsync();
        }

        public async ValueTask Add(string externalUserId, WeightMeasurement newMeasurement)
        {
            var userProfile = await dbContext.Profiles.SingleOrDefaultAsync(p => p.ExternalUserId == externalUserId);
            newMeasurement.UserProfile = userProfile;
            await dbContext.Measurements.AddAsync(newMeasurement);
            await dbContext.SaveChangesAsync();
        }
    }
}