using System.Threading.Tasks;
using WeightTracker.Persistence.Models;

namespace WeightTracker.Persistence
{
    public interface IUserProfileRepository
    {
        Task<UserProfile> Get(string externalUserId);
        Task Upsert(UserProfile userProfile);
    }
}