using System.Collections.Generic;
using System.Threading.Tasks;
using WeightTracker.Persistence.Models;

namespace WeightTracker.Persistence
{
    public interface IMeasurementsRepository
    {
        Task<IEnumerable<WeightMeasurement>> Get(string externalUserId, int skip = 0, int take = 100);
        ValueTask Add(string externalUserId, WeightMeasurement newMeasurement);
    }
}