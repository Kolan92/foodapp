using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace WeightTracker.Persistence
{
    internal class WeightTrackerDbContextFactory: IDesignTimeDbContextFactory<WeightTrackerDbContext>
    {
        public WeightTrackerDbContext CreateDbContext(string[] args)
        {
            // Build config
            IConfiguration config = new ConfigurationBuilder()
                .SetBasePath(Path.Combine(Directory.GetCurrentDirectory(), "../WeightTracker.WebApi"))
                .AddJsonFile("appsettings.json")
                .Build();

            // Get connection string
            var optionsBuilder = new DbContextOptionsBuilder<WeightTrackerDbContext>();
            var connectionString = config.GetConnectionString("DefaultConnection");
            optionsBuilder.UseNpgsql(connectionString);

            return new WeightTrackerDbContext(optionsBuilder.Options);
        }
    }
}