using Microsoft.EntityFrameworkCore;
using WeightTracker.Persistence.Models;

namespace WeightTracker.Persistence
{
    public class WeightTrackerDbContext : DbContext
    {
        public DbSet<UserProfile> Profiles { get; set; }
        public DbSet<WeightMeasurement> Measurements { get; set; }

        public WeightTrackerDbContext(DbContextOptions<WeightTrackerDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserProfile>()
                .HasIndex(profile => profile.ExternalUserId)
                .IsUnique();
            
            modelBuilder.Entity<UserProfile>()
                .HasMany(c => c.Measurements)
                .WithOne(e => e.UserProfile)
                .OnDelete(DeleteBehavior.Cascade)
                .IsRequired();

        }
    }
}