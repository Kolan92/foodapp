using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using WeightTracker.Persistence;
using WeightTracker.Service.Models;
using WeightTracker.WebApi;
using WeightMeasurement = WeightTracker.Service.Models.WeightMeasurement;

namespace WeightTracker.Service
{
    internal sealed class MeasurementsService : IMeasurementsService
    {
        private readonly IHttpContextAccessor httpContextAccessor;
        private readonly IMeasurementsRepository measurementsRepository;
        
        public MeasurementsService(IHttpContextAccessor httpContextAccessor, IMeasurementsRepository measurementsRepository)
        {
            this.httpContextAccessor = httpContextAccessor;
            this.measurementsRepository = measurementsRepository;
        }

        public async Task<IEnumerable<WeightMeasurement>> Get(int skip, int take)
        {
            var userId = httpContextAccessor.GetUserId() ?? throw new Exception("No user id in http context");

            var measurements =  await measurementsRepository.Get(userId, skip, take);
            return measurements
                .Select(m => new WeightMeasurement
                {
                    Id = m.Id,
                    MeasuredOn = m.MeasuredOn,
                    Weight = m.Weight
                });

        }

        public async Task<WeightMeasurement> Add(NewWeightMeasurement newWeightMeasurement)
        {
            var userId = httpContextAccessor.GetUserId() ?? throw new Exception("No user id in http context");
            var newEntry = new WeightTracker.Persistence.Models.WeightMeasurement
            {
                Weight = newWeightMeasurement.Weight,
                MeasuredOn = newWeightMeasurement.MeasuredOn
            };
            
            await measurementsRepository.Add(userId, newEntry);
            
            return new WeightMeasurement
            {
                Id = newEntry.Id,
                Weight = newEntry.Weight,
                MeasuredOn = newEntry.MeasuredOn
            };
        }
    }
}