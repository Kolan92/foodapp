using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using WeightTracker.Persistence;
using WeightTracker.WebApi;
using UserProfile = WeightTracker.Service.Models.UserProfile;

namespace WeightTracker.Service
{
    internal sealed class ProfileService : IProfileService
    {
        private readonly IHttpContextAccessor httpContextAccessor;
        private readonly IUserProfileRepository profileRepository;

        public ProfileService(IHttpContextAccessor httpContextAccessor, IUserProfileRepository profileRepository)
        {
            this.httpContextAccessor = httpContextAccessor;
            this.profileRepository = profileRepository;
        }

        public async Task<UserProfile> Get()
        {
            var userId = httpContextAccessor.GetUserId() ?? throw new Exception("No user id in http context");
            var profile = await profileRepository.Get(userId);
            
            return profile == null ? null : new UserProfile
            {
                Height = profile.Height,
                BirthDate = profile.BirthDate
            };
        }

        public async Task CreateOrUpdate(UserProfile profile)
        {
            var userId = httpContextAccessor.GetUserId() ?? throw new Exception("No user id in http context");
            var persistenceModel = new Persistence.Models.UserProfile
            {
                Height = profile.Height,
                BirthDate = profile.BirthDate,
                ExternalUserId = userId
            };

            await profileRepository.Upsert(persistenceModel);
        }
    }
}