using System.Threading.Tasks;
using WeightTracker.Service.Models;

namespace WeightTracker.Service
{
    public interface IProfileService
    {
        Task<UserProfile> Get();
        Task CreateOrUpdate(UserProfile profile);
    }
}