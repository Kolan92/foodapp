using System;

namespace WeightTracker.Service.Models
{
    public class UserProfile
    {
        public double Height { get; set; }
        public DateTime BirthDate { get; set; }
    }
}