using System;

namespace WeightTracker.Service.Models
{
    public class WeightMeasurement
    {
        public int Id { get; set; }
        public DateTime MeasuredOn { get; set; }
        public double Weight { get; set; }
    }
    
    public class NewWeightMeasurement
    {
        public DateTime MeasuredOn { get; set; }
        public double Weight { get; set; }
    }
}