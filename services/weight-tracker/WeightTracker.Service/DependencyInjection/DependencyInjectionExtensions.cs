using Microsoft.Extensions.DependencyInjection;

namespace WeightTracker.Service.DependencyInjection
{
    public static class DependencyInjectionExtensions
    {
        public static void AddServiceDependencies(this IServiceCollection services)
        {
            services.AddTransient<IProfileService, ProfileService>();
            services.AddTransient<IMeasurementsService, MeasurementsService>();
        }
    }
}