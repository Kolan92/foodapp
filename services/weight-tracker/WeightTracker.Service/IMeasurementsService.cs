using System.Collections.Generic;
using System.Threading.Tasks;
using WeightTracker.Service.Models;

namespace WeightTracker.Service
{
    public interface IMeasurementsService
    {
        Task<IEnumerable<WeightMeasurement>> Get(int skip, int take);
        Task<WeightMeasurement> Add(NewWeightMeasurement mappedEntry);
    }
}