using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WeightTracker.Service;
using WeightTracker.Service.Models;

namespace WeightTracker.WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class ProfileController: ControllerBase
    {
        private readonly IProfileService profileService;

        public ProfileController(IProfileService profileService)
        {
            this.profileService = profileService;
        }

        [HttpPost]
        public async Task<IActionResult> CreateOrUpdate(UserProfile userProfile)
        {
            await profileService.CreateOrUpdate(userProfile);
            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var profile = await profileService.Get();
            if (profile is null)
                return NotFound();
            return Ok(profile);
        }
    }
}