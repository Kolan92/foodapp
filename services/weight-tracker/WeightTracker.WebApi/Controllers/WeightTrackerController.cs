﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WeightTracker.Service;
using WeightTracker.Service.Models;

namespace WeightTracker.WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class WeightTrackerController : ControllerBase
    {
        private readonly IMeasurementsService measurementsService;

        public WeightTrackerController(IMeasurementsService measurementsService)
        {
            this.measurementsService = measurementsService;
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery]int skip = 0, [FromQuery]int take = 100)
        {
            var entries = await measurementsService.Get(skip, take);
            return Ok(entries);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody, Required] NewWeightMeasurement newEntry)
        {
            var createdMeasurement = await measurementsService.Add(newEntry);

            return StatusCode(201, createdMeasurement);
        }
        
    }
}