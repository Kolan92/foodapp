using System;
using System.Collections.Generic;
using WeightTracker.Persistence;
using WeightTracker.Persistence.Models;

namespace WeightTracker.WebApi.Models
{
    public class UserProfile
    {
        public string Id { get; set; }
        public double Height { get; set; }
        public DateTime BirthDate { get; set; }
        public ICollection<WeightMeasurement> Measurements { get; set; }
    }
}